#!/usr/bin/env python3

import requests
import sys
import time
import re
from bottle import route, run, request, template, static_file # Web framework
from bs4 import BeautifulSoup # XML and HTML scraping
import praw

wikiTitle = '''
    <title>wiki page tracklist</title>
'''

top64Title = '''
    <title>top 64 comments</title>
'''

top32Title = '''
    <title>top 32 comments</title>
'''

footer = '''
    <footer>
        <p><em>Get the source code for this website <a href="https://gitlab.com/KevinThomas0/music_survivor" target="_blank">here</a>.</em></p>
    </footer>
'''

@route('/favicon.ico', method='GET')
def get_favicon():
    return static_file('favicon.ico', root='./')

@route('/')
def home():
    return '''
        <!DOCTYPE html>
        <html>
        <title>music_survivor utilities</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <body>
            <h1>Get tracklist from wiki page</h1>
            <form action="/tracklist" method="post">
                Wiki Page: <input name="wikipage" type="text" />
                <input value="Submit" type="submit" />
            </form>
            <h1>Get top 64 comments from nomination thread</h1>
            <h3>(Disable contest mode on the thread before running this)</h3>
            <form action="/top64" method="post">
                Nomination Thread: <input name="nomination-thread" type="text" />
                <input value="Submit" type="submit" />
            </form>
            <h1>Paste your 64 options line by line to get code for google bracket generation</h1>
            <form action="/bracket-generation-64" method="post">
                Bracket Title: <input name="bracket-name" placeholder="(Ex: Best Title Track)" type="text" />
                <br><br>
                <textarea name="64-options" placeholder="(Paste your 64 entries here, one per line)" rows="25" cols="100"></textarea>
                <br><br>
                <input value="Submit" type="submit" />
            </form>
            <h1>Get top 32 comments from nomination thread</h1>
            <h3>(Disable contest mode on the thread before running this)</h3>
            <form action="/top32" method="post">
                Nomination Thread: <input name="nomination-thread" type="text" />
                <input value="Submit" type="submit" />
            </form>
            <footer>
                <p><em>Get the source code for this website <a href="https://gitlab.com/KevinThomas0/music_survivor" target="_blank">here</a>.</em></p>
            </footer>
        </body>
        </html>
    '''

@route('/tracklist', method='POST')
def tracklist():
    # URL of wiki page to scrape tracklist, grabbed from HTML form
    url = str(request.forms.getunicode('wikipage'))

    s = requests.Session()
    response = s.get(url, timeout=10)

    # Parse response content to html
    soup = BeautifulSoup(response.content, 'html.parser')

    # Older albums use different tables for different sides of the record
    # so this is a check for that condition
    if soup.find('table', {"class":'tracklist'}).find('caption') != None and "side" in str.lower(soup.find('table', {"class":'tracklist'}).find('caption').text):
        # Get tracklist table
        tables = soup.findAll('table', {"class":'tracklist'})

        # List of track titles
        titles = []

        # Loop through the multiple tracklist tables
        for table in tables:
            # Ignore tables that don't have a caption like "Side One"
            if "side" not in str.lower(table.find('caption').text):
                continue
            
            # List of track titles
            list_row = []
            for row in table.findAll("tr"):
                list_row.append(row)

            for i in range(len(list_row) - 1):
                title = list_row[i + 1].find('td').text.strip('\"')

                # Ignore "Total Length" row if present
                if "total length" in str.lower(list_row[i + 1].find('th').text):
                    continue
            
                titles.append("* " + title)

        # Join a newline to each track
        return "<br/>".join(map(str, titles))
    else:
        # Get tracklist table
        table = soup.find('table', {"class":'tracklist'})

        # HTML of each table record
        list_row = []
        for row in table.findAll("tr"):
            list_row.append(row)

        # List of track titles
        titles = []
        titles.append(wikiTitle)
        for i in range(len(list_row) - 1):
            title = list_row[i + 1].find('td').text.strip('\"')

            # Ignore "Total Length" row if present
            if "total length" in str.lower(list_row[i + 1].find('th').text):
                continue
            
            titles.append("* " + title)

        titles.append(footer)

        # Join a newline to each track
        return "<br/>".join(map(str, titles))

@route('/top64', method='POST')
def top64():
    # URL of reddit nomination thread
    url = str(request.forms.getunicode('nomination-thread'))

    reddit = praw.Reddit("music-survivor-app", user_agent="linux:dev.kevinthomas.ms (by u/dadrophenia)")

    submission = reddit.submission(url=url)
    submission.comment_sort = 'top'

    results = []
    results.append(top64Title)
    for i in range (64):
        results.append(submission.comments[i].body)

    results.append(footer)

    return "<br/>".join(map(str, results))

@route('/top32', method='POST')
def top32():
    # URL of reddit nomination thread
    url = str(request.forms.getunicode('nomination-thread'))

    reddit = praw.Reddit("music-survivor-app", user_agent="linux:dev.kevinthomas.ms (by u/dadrophenia)")

    submission = reddit.submission(url=url)
    submission.comment_sort = 'top'

    results = []
    results.append(top32Title)
    for i in range (32):
        results.append(submission.comments[i].body)

    results.append(footer)

    return "<br/>".join(map(str, results))

@route('/bracket-generation-64', method='POST')
def bracketgen64():
    optionsText = str(request.forms.getunicode('64-options'))
    bracketName = str(request.forms.getunicode('bracket-name')) 

    rawOptions = optionsText.splitlines()
    options = []
    for o in rawOptions:
        options.append(o.replace("'", "\\'"))

    return template('bracket-generation-64', bracketName=bracketName,
        option1=options[0],
        option2=options[1],
        option3=options[2],
        option4=options[3],
        option5=options[4],
        option6=options[5],
        option7=options[6],
        option8=options[7],
        option9=options[8],
        option10=options[9],
        option11=options[10],
        option12=options[11],
        option13=options[12],
        option14=options[13],
        option15=options[14],
        option16=options[15],
        option17=options[16],
        option18=options[17],
        option19=options[18],
        option20=options[19],
        option21=options[20],
        option22=options[21],
        option23=options[22],
        option24=options[23],
        option25=options[24],
        option26=options[25],
        option27=options[26],
        option28=options[27],
        option29=options[28],
        option30=options[29],
        option31=options[30],
        option32=options[31],
        option33=options[32],
        option34=options[33],
        option35=options[34],
        option36=options[35],
        option37=options[36],
        option38=options[37],
        option39=options[38],
        option40=options[39],
        option41=options[40],
        option42=options[41],
        option43=options[42],
        option44=options[43],
        option45=options[44],
        option46=options[45],
        option47=options[46],
        option48=options[47],
        option49=options[48],
        option50=options[49],
        option51=options[50],
        option52=options[51],
        option53=options[52],
        option54=options[53],
        option55=options[54],
        option56=options[55],
        option57=options[56],
        option58=options[57],
        option59=options[58],
        option60=options[59],
        option61=options[60],
        option62=options[61],
        option63=options[62],
        option64=options[63]
        )

run(host='0.0.0.0', port=8081)
